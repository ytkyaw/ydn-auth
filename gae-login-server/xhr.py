#
# @license Copyright 2012 YDN Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS-IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

from urlparse import urlparse
from google.appengine.ext import webapp
from setting import ALLOW_HOSTS


class XhrRequestHandler(webapp.RequestHandler):
	"""Cross Origin enable Request handler
	"""

	def initialize(self, request, response):
		super(XhrRequestHandler, self).initialize(request, response)

		origin = request.headers.get('Origin')
		if urlparse(origin).hostname in ALLOW_HOSTS:
			response.headers['Access-Control-Allow-Origin'] = origin
		response.headers['Access-Control-Allow-Credentials'] = 'true'
		response.headers['Access-Control-Allow-Headers'] = 'Content-Type,GData-Version,If-Match,If-Not-Match'
		response.headers['Access-Control-Allow-Methods'] = 'GET,POST,PUT,DELETE,OPTIONS,HEAD'
		response.headers['Cache-Control'] = 'no-cache'

	def head(self, *args):
		self.response.set_status(200)

	def options(self, *args):
		self.response.set_status(200)