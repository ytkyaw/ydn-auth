#
# @license Copyright 2012 YDN Authors. All Rights Reserved.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http:www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS-IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import json
from urlparse import urlparse
from google.appengine.api import users
from google.appengine.ext import webapp
from setting import ALLOW_HOSTS, DEBUG_FLAG
from xhr import XhrRequestHandler


class Redirect(webapp.RequestHandler):
    """Redirect after login HTML login
    """

    def get(self):
        page = str(self.request.get('page'))
        if urlparse(page).hostname not in ALLOW_HOSTS:
            self.response.set_status(401)
            return
        self.redirect(page)

class Login(XhrRequestHandler):
    """CORS login request handler
    """
    def get(self):
        user = users.get_current_user()
        output = {}
        output_user = {}
        redirect_url = self.request.get('url')
        if not redirect_url:
            redirect_url = self.request.url
        if urlparse(redirect_url).hostname not in ALLOW_HOSTS:
            self.response.set_status(401)
            return
        redirect_url = '/rpc_redirect?page=' + redirect_url
        if user:
            output_user['email'] = user.email()
            output_user['logout_url'] = users.create_logout_url(redirect_url)
        else:
            output_user['login_url'] = users.create_login_url(redirect_url)

        output['User'] = output_user
        self.response.out.write(json.dumps(output))


class MainHandler(webapp.RequestHandler):
    def get(self):
        self.response.out.write('<h1>YDN GAE login server</h1>' +
                                '<p><a href="https://bitbucket.org/ytkyaw/ydn-auth">Source code</a></p>' +
                                '<p><a href="http://dev.yathit.com">Yathit Developers Center</a></p>')


app = webapp.WSGIApplication([
    ('/rpc_login', Login),
    ('/rpc_redirect', Redirect),
    ('/', MainHandler)], debug=DEBUG_FLAG)

