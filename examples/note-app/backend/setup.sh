#!/bin/sh

export BK=ydn-note-data

gsutil mb gs://$BK
gsutil setacl acl.xml gs://$BK
gsutil setdefacl default-acl.xml gs://$BK
gsutil setcors cors.xml   gs://$BK
gsutil cp -z html proxy.html gs://$BK/proxy.html