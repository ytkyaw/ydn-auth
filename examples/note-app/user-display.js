/**
 * @fileoverview User display.
 */



/**
 * User display.
 * @constructor
 */
var UserDisplay = function() {
  this.a_login_ = document.getElementById('user-login');
  this.span_nickname_ = document.getElementById('user-name');
};


/**
 * @param {string} url
 */
UserDisplay.prototype.setLoginUrl = function(url) {
  this.a_login_.textContent = 'login';
  this.a_login_.href = url;
};


/**
 * @param {string} url
 */
UserDisplay.prototype.setLogoutUrl = function(url) {
  this.a_login_.textContent = 'logout';
  this.a_login_.href = url;
};


/**
 * @param {string} nickname
 */
UserDisplay.prototype.setNickname = function(nickname) {
  this.span_nickname_.textContent = nickname;
  this.span_nickname_.style.display = !!nickname ? '' : 'none';
};


/**
 * @return {EventType}
 */
UserDisplay.prototype.getLoginClick = function() {
  return this.a_login_;
};
