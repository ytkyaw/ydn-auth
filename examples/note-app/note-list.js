/*
 * Copyright 2013, YDN Authors, Yathit.
 *
 * The code should be distributed under the LGPL license.
 * See http://www.gnu.org/licenses/lgpl.html for details.
 */


/**
 * @fileoverview Note list controller and display.
 */



/**
 * Create a note lis.
 * @param {Element} parent_ele
 * @param {ydn.db.Storage} db storage.
 * @param {NoteEditor} editor
 * @param {string} store_name
 * @param {string} prefix
 * @constructor
 */
var NoteList = function(parent_ele, db, editor, store_name, prefix) {
  /**
   * @type {Element}
   * @private
   * @final
   */
  this.root_ = document.createElement('DIV');
  parent_ele.appendChild(this.root_);
  this.editor = editor;
  /**
   * @type {string}
   * @private
   * @final
   */
  this.prefix_ = prefix;
  /**
   * @type {string}
   * @private
   * @final
   */
  this.store_name_ = store_name;
  /**
   * @type {ydn.db.Storage}
   * @private
   * @final
   */
  this.db_ = db;

  var me = this;
  this.root_.addEventListener('click', function(e) {
    var ele = e.target;
    if (ele.tagName == 'LI') {
      var key = ele.name;
      me.db_.get(me.store_name_, key).then(function(record) {
        me.editor.setData(new NoteData(record));
      }, function(e) {
        throw e;
      });
    }
  });
  this.last_refresh_ = null;
  this.refreshing_ = false;
};


/**
 * Fetch note list and update display.
 * @private
 */
NoteList.prototype.delay_refresh_ = function() {
  var key_range = this.prefix_ ? ydn.db.KeyRange.starts(this.prefix_) : null;
  // window.console.log('refershing');
  this.db_.values(this.store_name_, key_range, 10, 0, true).then(
      function(list) {
        this.render_(list);
      }, function(e) {
        throw e;
      }, this);
  this.last_refresh_ = new Date();
};


/**
 * Fetch note list and update display.
 */
NoteList.prototype.refresh = function() {
  var interval = 1000; // at least one second throttle.
  if (!this.last_refresh_ || (new Date() - this.last_refresh_ > interval)) {
    this.delay_refresh_();
  } else {
    var me = this;
    if (!this.refreshing_) {  // not repeatly
      setTimeout(function() {
        me.delay_refresh_();
        me.refreshing_ = false;
      }, interval);
    }
    this.refreshing_ = true;
  }
};


/**
 * @param {Array} list list of nodes.
 * @private
 */
NoteList.prototype.render_ = function(list) {

  var ul = document.createElement('UL');
  ul.style.listStyle = 'none';
  for (var i = 0, n = list.length; i < n; i++) {
    var li = document.createElement('LI');
    li.style.cursor = 'pointer';
    var note = new NoteData(list[i]);
    li.textContent = note.getLabel();
    li.name = note.getKey();
    ul.appendChild(li);
  }

  if (this.root_.childNodes[0]) {
    this.root_.replaceChild(ul, this.root_.childNodes[0]);
  } else {
    this.root_.appendChild(ul);
  }
};

