/**
 * @fileoverview Note application using google cloud storage service.
 */


var bucket = 'ydn-test-crud-1';
var store_name = 'note';
var options = {
  appId: 'test',
  loginServerOrigin: 'https://yit204.appspot.com',
  authServerOrigin: 'https://yit204.appspot.com',
  authorize: [
    {
      type: 'sign',
      bucket: bucket,
      context: 'test-sync'
    }],
  AppSchema: {
    stores: [
      {
        name: store_name,
        keyPath: 'Key',
        Sync: {
          format: 'gsc',
          fetchStrategies: ['descending-key'],
          Options: {
            bucket: bucket
          }
        }
      }
    ]
  }
};

ydn.debug.log('ydn.db.sql.req', 'finest');
ydn.debug.log('ydn.db.sync', 'finest');

var app = new ydn.api.App(options);
var user = app.getUser();

var user_display = new UserDisplay();
user.setView(user_display);

app.init(); // restore session data.
var db = app.getAppDb();

var base_ele = document.getElementById('base');
var editor = new NoteEditor(base_ele, db);
var list = new NoteList(base_ele, db, editor, store_name);
list.refresh();
db.addEventListener(['delay-read', 'delay-updated', 'delay-insert',
  'delay-delete'], function(ev) {
  window.console.log('refresh for ' + ev.type);
  list.refresh();
});

user.addEventListener('user-login', function(e) {
  editor.setVisible(true);
});

app.run(); // connect to login server
window.app = app; // let global
app.list = list;
