/**
 * @fileoverview Note data.
 */



/**
 * Represent note record.
 * @param {Object?} record
 * @constructor
 */
var NoteData = function(record) {
  this.data_ = record;

};


/**
 * Return record key.
 * @return {string?}
 */
NoteData.prototype.getKey = function() {
  return this.data_ ? this.data_.Key : null;
};


/**
 * Return record content.
 * @return {string?}
 */
NoteData.prototype.getContent = function() {
  return this.data_ ? this.data_['Content'] : null;
};


/**
 * Save note.
 * @param {string} text content text.
 * @return {jQuery.Deferred}
 */
NoteData.prototype.setContent = function(text) {

  var me = this;
  if (!this.data_ || !this.data_.Key) {
    var epoch = new Date(9999, 0, 1);
    var id = 'note/' + (epoch - new Date());
    this.data_ = {
      Key: id,
      Content: text
    };
    var df = window.app.getAppDb().add('note', this.data_);
    df.then(function(key) {
      me.listener('added', me);
    }, function(e) {
      throw e;
    });
    return df;
  } else {
    this.data_['Content'] = text;
    var df2 = window.app.getAppDb().put('note', this.data_);
    df2.then(function(key) {
      window.console.log(key + ' updated.');
      me.listener('updated', me);
    }, function(e) {
      throw e;
    });
    return df2;
  }
};


/**
 * @return {jQuery.Deferred}
 */
NoteData.prototype.delete = function() {
  if (this.data_) {
    var key = this.data_.Key;
    window.console.assert(key);
    var me = this;
    var df = window.app.getAppDb().remove('note', key);
    df.then(function(cnt) {
      if (cnt == 1) {
        me.listener('deleted', me);
        me.data_ = null;
      } else {
        window.console.warn('Fail to delete ' + key);
      }
    }, function(e) {
      throw e;
    });
    return df;
  } else {
    window.console.warn('Nothing to delete');
    throw new Error('no data');
  }
};


/**
 * Event listener.
 * @param {string} type event type.
 * @param {EventTarget} target event target.
 */
NoteData.prototype.listener = function(type, target) {
  setTimeout(function() {
    window.console.log('event ' + type + ' for ' + target);
    list.refresh();
  }, 1000);

};


/**
 * Get note label.
 * @return {string}
 */
NoteData.prototype.getLabel = function() {
  if (this.data_) {
    var text = this.data_.Key + ' ' + (this.data_.Content || '');
    if (text.length > 70) {
      text = text.substr(0, 70);
    }
    return text;
  } else {
    return '';
  }
};


/**
 * @override
 * @return {string}
 */
NoteData.prototype.toString = function() {
  return 'NoteData:' + this.getKey();
};


