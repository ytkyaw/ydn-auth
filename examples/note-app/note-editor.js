/**
 * @fileoverview Edit note content.
 */



/**
 *
 * @param {Element} parent_ele
 * @constructor
 */
var NoteEditor = function(parent_ele) {
  this.root_ele_ = document.createElement('DIV');

  var btn_new_ = document.createElement('BUTTON');
  btn_new_.textContent = 'New';
  var btn_save_ = document.createElement('BUTTON');
  btn_save_.textContent = 'Save';
  this.btn_delete_ = document.createElement('BUTTON');
  this.btn_delete_.textContent = 'Delete';
  this.div_toolbar_ = document.createElement('DIV');
  this.root_ele_.appendChild(btn_new_);
  this.div_toolbar_.appendChild(btn_save_);
  this.div_toolbar_.appendChild(this.btn_delete_);
  this.div_toolbar_.style.display = 'none';
  this.textarea_ = document.createElement('TEXTAREA');
  this.txt_key_ = document.createElement('DIV');
  this.root_ele_.appendChild(this.txt_key_);
  this.root_ele_.appendChild(this.textarea_);
  this.root_ele_.appendChild(this.div_toolbar_);
  parent_ele.appendChild(this.root_ele_);

  this.setData(new NoteData());

  var me = this;
  btn_save_.addEventListener('click', function(e) {
    me.data_.setContent(me.textarea_.value).done(function(key) {
      me.txt_key_.textContent = key;
      me.btn_delete_.style.display = '';
    });

  });
  btn_new_.addEventListener('click', function(e) {
    me.setData(new NoteData());
  });
  this.btn_delete_.addEventListener('click', function(e) {
    me.data_.delete().done(function(cnt) {
      me.setData(new NoteData());
    });
  });
};


/**
 * @type {Element}
 * @private
 */
NoteEditor.prototype.root_ele_;


/**
 * @type {Object}
 */
NoteEditor.prototype.data_;


/**
 *
 * @param {boolean} v
 */
NoteEditor.prototype.setVisible = function(v) {
  // console.log('setting visible to ' + v);
  this.div_toolbar_.style.display = v ? '' : 'none';
};


/**
 *
 * @param {NoteData} record
 */
NoteEditor.prototype.setData = function(record) {
  this.data_ = record;
  var key = record.getKey();
  this.txt_key_.textContent = key || '';
  this.btn_delete_.style.display = key ? '' : 'none';
  this.textarea_.value = record.getContent() || '';
};



